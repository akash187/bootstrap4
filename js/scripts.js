jQuery(function () {
    $("#mycarousel").carousel({interval: 2000});
    $("#carousel-button").click(function () {
        var selector = "#carousel-button";
        if ($(selector).children("span").hasClass('fa-pause')) {
            $("#mycarousel").carousel('pause');
            $(selector).children("span").removeClass('fa-pause');
            $(selector).children("span").addClass('fa-play');
        }
        else if ($(selector).children("span").hasClass('fa-play')) {
            $("#mycarousel").carousel('cycle');
            $(selector).children("span").removeClass('fa-play');
            $(selector).children("span").addClass('fa-pause');
        }
    });
});

$("#bookTableModal-Button").click(function(){
    $('#bookTableModal').modal('show');
});
$("#loginModal-button").click(function(){
    $('#loginModal').modal('show');
});